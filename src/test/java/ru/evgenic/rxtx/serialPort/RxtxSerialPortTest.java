/*
 * Тестовый класс для библиотеки работы с портом
 */
package ru.evgenic.rxtx.serialPort;

import gnu.io.SerialPortEvent;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;


/**
 * Тестовый класс для библиотеки работы с портом.
 * Содержит два теста на отпраку и прием.
 * Активный тест должен быть ОДИН, чтоб не конфликтовал захват порта.
 * @author egorov
 */
public class RxtxSerialPortTest {

    public RxtxSerialPortTest() {
    }

    /**
     * Скорость порта = 9600
     *   Биты данных = 8
     *   Четность = 2
     *   Стоповые биты = 2
     * @throws Exception
     */
    @Ignore
    @Test
    public void testSendToPort() throws Exception {
        System.out.println("Send to Port");
        // в конструктор передать название порта, например COM1 под win или /dev/ttyS0 под nix
        ISerialPort port = new RxtxSerialPort("COM1");
        port.setSpeed(9600);
        port.setStopBits(2);
        port.setParity(0);
        byte[] b = new byte[7];
        b[0] = 1;
        b[1] = 33;
        b[2] = 0;
        b[3] = '1';
        b[4] = '2';
        b[5] = '3';
        b[6] = 7;
        port.send(b);
        System.out.println("If you see this message \"Test Message\" on device then all right! Else something bad.");
    }

    @Ignore
    @Test
    public void testReceiveFromPort() throws Exception {
        System.out.println("Receive from Port");
        // в конструктор передать название порта, например COM1 под win или /dev/ttyS0 под nix
        ISerialPort port = new RxtxSerialPort("COM1");
        port.setDataBits(8);
        port.setParity(0);
        port.setStopBits(1);

        port.bind(new IReceiveListener() {

            @Override
            public void actionPerformed(SerialPortEvent event, byte[] data) {
                rData = new String(data);
                System.out.println("Receive message from Port: " + rData);
            }

            @Override
            public void actionPerformed(SerialPortEvent event) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        IReceiveListener li = new IReceiveListener() {

            @Override
            public void actionPerformed(SerialPortEvent event, byte[] data) {
                rData = new String(data);
                System.out.println("Listener removed: " + rData);
            }

            @Override
            public void actionPerformed(SerialPortEvent event) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };

        port.addListener(li);

        port.addListener(new IReceiveListener() {

            @Override
            public void actionPerformed(SerialPortEvent event, byte[] data) {
                rData = new String(data);
                System.out.println("Listener1: " + rData);
            }

            @Override
            public void actionPerformed(SerialPortEvent event) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        port.addListener(new IReceiveListener() {

            @Override
            public void actionPerformed(SerialPortEvent event, byte[] data) {
                rData = new String(data);
                System.out.println("Listener2: " + rData);
            }

            @Override
            public void actionPerformed(SerialPortEvent event) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        port.removeListener(li);

        port.free();

        port.bind(new IReceiveListener() {

            @Override
            public void actionPerformed(SerialPortEvent event, byte[] data) {
                rData = new String(data);
                System.out.println("Rebind 1: " + rData);
            }

            @Override
            public void actionPerformed(SerialPortEvent event) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        port.bind(new IReceiveListener() {

            @Override
            public void actionPerformed(SerialPortEvent event, byte[] data) {
                rData = new String(data);
                System.out.println("Rebind final: " + rData);
            }

            @Override
            public void actionPerformed(SerialPortEvent event) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });

        System.out.println("If you see message from device then all right! Else something bad.");
        System.out.println("10 second for scan.");
        int i = 0;
        while ("".equals(rData) && i < 100) {
            Thread.sleep(100);
            i++;
        }
        if (i == 100) {
            System.out.println("NO receive data. May be problem");
        } else {
            System.out.println("Receive data : " + rData);
        }
        port.free();
        port.free();
        port.free();
    }
    private static String rData = "";
}
