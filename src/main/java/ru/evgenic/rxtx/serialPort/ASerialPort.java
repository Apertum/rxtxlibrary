/*
 *  Copyright (C) 2010 Apertum project. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.evgenic.rxtx.serialPort;

import gnu.io.SerialPortEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.SynchronousQueue;

/**
 * RU:
 * Абстрактный класс реализации для работы с COM-портом.
 * Обозначены основные методы работы с портом.
 * <br>
 * EN:
 * Abstract class of realisation for work with COM-port
 * The basic methods of work with port are designated.
 * @version 1.1.2
 * @author Evgeniy Egorov
 */
abstract public class ASerialPort implements ISerialPort, Runnable {

    public ASerialPort() {
        serialPortName = "";
    }
    /**
     * RU:
     * Название порта.
     * <br>
     * EN:
     * The port name
     */
    protected String serialPortName;
    private ISerialLoggerListener serialLoggerListener = null;

    @Override
    public void setLoggerListener(ISerialLoggerListener serialLoggerListener) {
        this.serialLoggerListener = serialLoggerListener;
    }

    protected void getLoggerListener(String message, boolean isError) {
        if (serialLoggerListener != null) {
            serialLoggerListener.actionPerformed(message + (isError ? new Exception().getStackTrace() : ""), isError);
        } else {
            System.out.println(message + (isError ? new Exception().getStackTrace() : ""));
        }
    }
    protected ISerialExceptionListener serialExceptionListener;

    @Override
    public void setExceptionListener(ISerialExceptionListener serialExceptionListener) {
        this.serialExceptionListener = serialExceptionListener;
    }

    protected void getExceptionListener(String message) throws Exception {
        if (serialExceptionListener != null) {
            serialExceptionListener.actionPerformed(message);
        } else {
            throw new Exception(message);
        }
    }
    /**
     * RU:
     * Состояние порта.
     * <br>
     * EN:
     * Port condition.
     */
    private boolean binded = false;

    private void setBinded(boolean state) {
        final String s;
        if (state) {
            s = "open";
        } else {
            s = "close";
        }
        binded = state;
        getLoggerListener("Port \"" + serialPortName + "\" was " + s, false);
    }

    protected boolean isBinded() {
        return binded;
    }

    /**
     * RU:
     * Получить идентификатор COM-порта, с которым работает класс.
     * <br>
     * EN:
     * To receive the COM-port identifier with which the class works.
     * @return RU: идентификатор COM-порта <br> EN: the COM-port identifier
     */
    public String getSerialPortName() {
        return serialPortName;
    }

    /**
     * RU:
     * Определение порта
     * <br>
     * EN:
     * Port definition
     * @param serialPortName RU: имя порта <br> EN: a port name
     * @throws Exception RU: исключение, если такой порт не найден в системе.<br> EN: an exception if such port is not found in system.
     */
    abstract protected void setSerialPortName(String serialPortName) throws Exception;
    /**
     * Листенеры, в которые передается событие приема данных.
     */
    protected HashMap<IReceiveListener, IReceiveListener> receiveListeners = new HashMap<IReceiveListener, IReceiveListener>();

    @Override
    public void addListener(IReceiveListener receiveListener) {
        if (receiveListener != null) {
            receiveListeners.put(receiveListener, receiveListener);
        }
    }

    @Override
    public void removeListener(IReceiveListener receiveListener) {
        receiveListeners.remove(receiveListener);
    }

    @Override
    public Collection<IReceiveListener> getListeners() {
        return receiveListeners.values();
    }
    /**
     * Листенер для получения данных из потока, в которые передается событие приема данных.
     */
    protected IReceiveStreamListener receiveStreamListeners = null;

    @Override
    public IReceiveStreamListener getReceiveStreamListeners() {
        return receiveStreamListeners;
    }

    @Override
    public void setReceiveStreamListeners(IReceiveStreamListener receiveStreamListeners) {
        this.receiveStreamListeners = receiveStreamListeners;
    }

    /**
     * RU:
     * Захватить порт для приема информации. Устанавливается Listener для события.
     * Если порт уже захвачен, то меняется только Listener.
     * Повторный захват не производится, он и не сможет быть захвачен повторно.
     * <br>
     * EN:
     * To grasp port for information reception. It is established Listener for event.
     * If the port is already grasped, varies only Listener.
     * Repeated capture is not made and cannot be grasped repeatedly.
     * @param receiveListener RU: Класс, в котором вызовется метод при приеме информации. <br> EN: the Class in which the method will be caused at information reception.
     * @throws java.lang.Exception not success
     */
    @Override
    public void bind(IReceiveListener receiveListener) throws Exception {
        getLoggerListener("Try to bind the port.", false);
        addListener(receiveListener);
        if (binded) {
            getLoggerListener("Try to rebind the port \"" + serialPortName + "\" is disable. Port have binded and alredy using.", false);
        } else {
            setBinded(getPort());
            if (!binded) {
                getExceptionListener("Potr " + serialPortName + " failed to bind.");
            }
        }
    }

    /**
     * RU:
     * Захватить порт для приема информации. Устанавливается Listener для события.
     * Если порт уже захвачен, то меняется только Listener.
     * Повторный захват не производится, он и не сможет быть захвачен повторно.
     * <br>
     * EN:
     * To grasp port for information reception. It is established Listener for event.
     * If the port is already grasped, varies only Listener.
     * Repeated capture is not made and cannot be grasped repeatedly.
     * @param listener RU: Класс, в котором вызовется метод при приеме информации. <br> EN: the Class in which the method will be caused at information reception.
     * @throws java.lang.Exception not success
     */
    @Override
    public void bind(IReceiveStreamListener listener) throws Exception {
        getLoggerListener("Try to bind the port.", false);
        setReceiveStreamListeners(listener);
        if (binded) {
            getLoggerListener("Try to rebind the port \"" + serialPortName + "\" is disable. Port have binded and alredy using.", false);
        } else {
            setBinded(getPort());
            if (!binded) {
                getExceptionListener("Potr " + serialPortName + " failed to bind.");
            }
        }
    }

    /**
     * RU:
     * Захватить порт.
     * <br>
     * EN:
     * To grasp port.
     * @return RU: результат захвата порта. <br> EN: result of capture of port.
     */
    abstract protected boolean getPort();

    /**
     * RU:
     * Освободить захваченный для прослушивания порт.
     * <br>
     * EN:
     * To release the port grasped for listening.
     * @see bind (IReceiveListener receiveListener);
     * @throws Exception RU: сключение при освобождении порта. EN: exeption for port free
     */
    @Override
    public void free() throws Exception {
        getLoggerListener("Try to free for potr \"" + serialPortName + "\".", false);
        freePort();
        setBinded(false);
    }

    /**
     * RU:
     * Освободить порт.
     * <br>
     * EN:
     * To release port.
     * @throws Exception RU:  Исключение при освобождении порта. EN: the Exception at port clearing.
     */
    abstract protected void freePort() throws Exception;

    /**
     * En: Send message
     * Ru: Отослать сообщение.
     * @param message En: Text message. <br> Ru: Сообшение.
     * @throws Exception not success
     */
    @Override
    public void send(byte[] message) throws Exception {
        Thread sendThread = new Thread(this);
        sendThread.setDaemon(true);
        sendThread.start();
        try {
            syncData.put(message);
        } catch (InterruptedException ex) {
            getExceptionListener("Error transmit data into thread for sending to port " + serialPortName + "\". " + ex.toString());
        }
    }
    /**
     * RU:
     * Через эту константу происходит синхронизированная передача данных в потоки для последующей их передачи в порт.
     * <br>
     * EN:
     * Through this constant there is a synchronised data transmission in streams for their subsequent transfer to port.
     */
    static final SynchronousQueue<byte[]> syncData = new SynchronousQueue<byte[]>();

    /**
     * RU:
     * Отослать сообщение.
     * <br>
     * EN:
     * Send message
     * @param message data for sending
     * @throws Exception  not success
     */
    abstract protected void sendMessage(byte[] message) throws Exception;

    @Override
    public void run() {
        try {
            final byte[] message = (byte[]) syncData.take();
            sender(message);
        } catch (Exception ex) {
            getLoggerListener("Error receive data in thread for sending to port " + serialPortName + "\". " + ex.toString(), true);
        }
    }

    synchronized private void sender(byte[] message) throws Exception {
        boolean needFree = true;
        if (!binded) {
            bind(new IReceiveListener() {

                @Override
                public void actionPerformed(SerialPortEvent event, byte[] data) {
                }

                @Override
                public void actionPerformed(SerialPortEvent event) {
                }
            });
        } else {
            needFree = false;
        }
        if (!binded) {
            getExceptionListener("Port " + serialPortName + " failed to bind.");
        }
        getLoggerListener("Send data to port  \"" + serialPortName + "\" Length of data " + message.length + " byte.", false);
        sendMessage(message);
        if (needFree) {
            free();
        }
    }
}
