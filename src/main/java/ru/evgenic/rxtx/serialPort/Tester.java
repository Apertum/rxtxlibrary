package ru.evgenic.rxtx.serialPort;

import gnu.io.CommPortIdentifier;

import java.util.Enumeration;

public class Tester {
    public static void main(String[] args) throws Exception {
        final Enumeration portList = CommPortIdentifier.getPortIdentifiers();
        while (portList.hasMoreElements()) {
            final CommPortIdentifier portIdTemp = (CommPortIdentifier) portList.nextElement();
            if (portIdTemp.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                System.out.println("Detected port: \"" + portIdTemp.getName() + "\" owner=" + portIdTemp.getCurrentOwner() + ", type=" + portIdTemp.getPortType());
            }
        }

        String portName = "COM1";
        String message = "1234567890";
        int speed = 115000;
        int parity = 2;
        int databits = 8;
        int stopbits = 1;
        for (int i = 0; i < args.length - 1; i++) {
            switch (args[i]) {
                case "-port":
                    portName = args[i + 1];
                    System.out.println("-port=" + portName);
                    break;
                case "-msg":
                    message = args[i + 1];
                    System.out.println("-msg=" + message);
                    break;
                case "-speed":
                    speed = Integer.parseInt(args[i + 1]);
                    System.out.println("-speed=" + speed);
                    break;
                case "-parity":
                    parity = Integer.parseInt(args[i + 1]);
                    System.out.println("-parity=" + parity);
                    break;
                case "-stopbits":
                    stopbits = Integer.parseInt(args[i + 1]);
                    System.out.println("-stopbits=" + stopbits);
                    break;
                case "-databits":
                    databits = Integer.parseInt(args[i + 1]);
                    System.out.println("-databits=" + databits);
                    break;
                default: {
                }
            }
        }

        System.out.println("-port=" + portName + " -msg=" + message + " -speed=" + speed + " -parity=" + parity + " -stopbits=" + stopbits + " -databits=" + databits);

        ISerialPort port = new RxtxSerialPort(portName);
        port.setSpeed(speed);
        port.setParity(parity);
        port.setStopBits(stopbits);
        port.setDataBits(databits);
        port.send(message.getBytes());
        port.free();
    }
}
