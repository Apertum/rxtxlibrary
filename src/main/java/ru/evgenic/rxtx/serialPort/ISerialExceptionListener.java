/*
 *  Copyright (C) 2010 Apertum project. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.evgenic.rxtx.serialPort;

/**
 * RU:
 * Листенер для обработки исключительных ситуаций при работе с СОМ портом.
 * Имеет единственный метод, в который передасться сообщение из события.
 * При необходимости обработки ошибок с портом инициализировать это событие вызвав метод
 * из ISerialPort public void setExceptionListener(ISerialExceptionListener serialExceptionListener);
 * <br>
 * EN:
 * Listener for processing of exclusive situations at work with the CATFISH port.
 * Has a unique method in which the message from event will be transferred.
 * In need of processing of errors with port to initialize this event having caused a method
 * From ISerialPort public void setExceptionListener (ISerialExceptionListener serialExceptionListener);
 * @version 1.0
 * @author Evgeniy Egorov
 */
public interface ISerialExceptionListener {

    /**
     * RU:
     * Выполняется при наступлении события ошибки.
     * <br>
     * EN:
     * It is carried out at approach of event of an error.
     * @param message  Сообщение о событии. the Message on event.
     * @throws Exception java.lang. RU: это действие выполняется в исключительных случаях, и при исключениях этот метод может генерировать исключение.  <br>EN: Exception this action is carried out in exceptional cases, and at exceptions this method can generate an exception.
     */
    public void actionPerformed(String message) throws Exception;
}
