/*
 *  Copyright (C) 2010 Apertum project. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.evgenic.rxtx.serialPort;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.TooManyListenersException;

/**
 * RU:
 * Работа с COM-портом используя библиотеку RXTX-2.1.7.
 * Создать объект этого класса и использовать методы интерфейса ISerialPort,
 * Например ISerialPort port = new RxtxSerialPort("COM1");
 * Другие методы этого класса не для использования.
 * Параметры порта по умолчанию:  <br>
 *   Скорость порта = 9600 <br>
 *   Биты данных = 8 <br>
 *   Четность = 2 <br>
 *   Стоповые биты = 2 <br>
 * <br>
 * EN:
 * Work with serial port using library RXTX-2.1.7
 * Create object of this class and use methods of interface ISerialPort
 * Example ISerialPort port = new RxtxSerialPort("COM1");
 * Other methods of this class are not for use
 * Port parametrs by default:  <br>
 * Port Speed = 9600 <br>
 * Data bit = 8 <br>
 * Parity = 2 <br>
 * Stop bit = 2 <br>
 * @author Evgeniy Egorov
 * @version 1.1.1
 */
public class RxtxSerialPort extends ASerialPort implements SerialPortEventListener {

    /**
     * RU:
     * Идентификатор порта, через который будут передаваться данные.
     * EN:
     * Port identificator which will be used for transferring data
     */
    private CommPortIdentifier portId;
    private InputStream inputStream;
    /**
     * RU:
     * COM - порт.
     * EN:
     * Serial port
     */
    private SerialPort serialPort;

    public SerialPort getSerialPort() {
        return serialPort;
    }

    public void setSerialPort(SerialPort serialPort) {
        this.serialPort = serialPort;
    }
    static OutputStream outputStream;
    static boolean outputBufferEmptyFlag = false;

    /**
     * Обязательный класса COM-порта. 
     * При создании ему передается идентификатор порта, с которым он впоследствии работает.
     * @param serialPortName имя используемого порта.
     * @throws java.lang.Exception Генерируется исключение, если не найден порт с именем "serialPortName".
     */
    public RxtxSerialPort(String serialPortName) throws Exception {
        getLoggerListener("Trying to determine the port \"" + serialPortName + "\".", false);
        setSerialPortName(serialPortName);
    }
    
    /**
     * Обязательный класса COM-порта. 
     * При создании ему передается идентификатор порта, с которым он впоследствии работает.
     * @param serialPortName имя используемого порта.
     * @param serialLoggerListener how to log
     * @param serialExceptionListener how to work with wxceptions
     * @throws java.lang.Exception Генерируется исключение, если не найден порт с именем "serialPortName".
     */
    public RxtxSerialPort(String serialPortName, ISerialLoggerListener serialLoggerListener, ISerialExceptionListener serialExceptionListener) throws Exception {
        setLoggerListener(serialLoggerListener);
        setExceptionListener(serialExceptionListener);
        getLoggerListener("Trying to determine the port \"" + serialPortName + "\".", false);
        setSerialPortName(serialPortName);
    }

    /**
     * RU:<br>
     * Маркировка СОМ-порта<br>
     * EN:<br>
     * Marking of serial port<br>
     * @return RU: строка названия СОМ-порта. <br> EN: String of name of serial port
     */
    @Override
    public String getName() {
        return serialPortName;
    }

    /**
     * RU:<br>
     * Инициализалия COM-порта. Если идентификатор правильный, то инициализация пройдет нормально.
     * Порт находится и запоминается. Дальнейшее его использование(открытие) начитается с getPort().<BR>
     * EN:<br>
     * Serial port initialization. If identificator is correct, then  initialization will goo normally.
     * Port was found and remembered. After that its use starts from getPort().
     * @param serialPortName RU: идентификатор порта, с которым класс он впоследствии работает. EN: is a port identificator which class uses for work
     * @throws java.lang.Exception RU: исключение, если такой порт не найден в системе. EN: is exeption if such port is non foun in system
     */
    @Override
    protected final void setSerialPortName(String serialPortName) throws Exception {
        boolean portFound = false;
        // Пробежимся по доступным портам и определим возможность использования требуемого порта
        final Enumeration portList = CommPortIdentifier.getPortIdentifiers();
        while (portList.hasMoreElements()) {
            final CommPortIdentifier portIdTemp = (CommPortIdentifier) portList.nextElement();
            if (portIdTemp.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                getLoggerListener("Detected port: " + portIdTemp.getName(), false);
                if (portIdTemp.getName().equals(serialPortName)) {
                    getLoggerListener("Port is initialized: " + serialPortName, false);
                    portFound = true;
                    // определим рабочий порт
                    portId = portIdTemp;
                    // выдим из цикла пробежки по всем портам
                    break;
                }
            }
        }
        if (!portFound) {
            getLoggerListener("Port " + serialPortName + " not found.", true);
            getExceptionListener("Port " + serialPortName + " not found.");
        }
        this.serialPortName = serialPortName;
    }

    /**
     * RU:<br>
     * Захватываем и инициализируем порт.<br>
     * EN:<br>
     * Capture and initialize port
     * @return  not success
     */
    @Override
    protected boolean getPort() {
        // initalize serial port
        try {
            try {
                serialPort = (SerialPort) portId.open("java.exe", 2000);
            } catch (PortInUseException e) {
                getExceptionListener("Port " + serialPortName + " could not be opened.  Possible is usesing. " + e.getMessage());
            }

            // выставим параметры порта.
            init();

            if (receiveListeners != null) {
                //ПРИЕМ
                try {
                    inputStream = serialPort.getInputStream();
                } catch (IOException e) {
                    getExceptionListener("Not possible to create a thread to receive data from port " + serialPortName + ". " + e.getMessage());
                }
                //ПРИЕМ
                try {
                    serialPort.addEventListener(this);
                } catch (TooManyListenersException e) {
                    getExceptionListener("Too many event handlers port " + serialPortName + "." + e.getMessage());
                }

                //ПРИЕМ activate the DATA_AVAILABLE notifier
                serialPort.notifyOnDataAvailable(true);
            }


            //ОТПРАВКА
            try {
                // get the outputstream
                outputStream = serialPort.getOutputStream();
            } catch (IOException e) {
                getExceptionListener("Unable to determine the output port " + serialPortName + "." + e.getMessage());
            }
            //ОТПРАВКА
            try {
                // activate the OUTPUT_BUFFER_EMPTY notifier
                serialPort.notifyOnOutputEmpty(true);
            } catch (Exception e) {
                getExceptionListener("Unable to set event port " + serialPortName + "." + e.toString());
            }
        } catch (Exception e) {
            getLoggerListener("Failed to capture the port: " + e.toString(), true);
            return false;
        }
        return true;

    }

    /**
     * RU:<br>
     * Инициализация порта
     * EN:<br>
     * Port initialization
     * @throws Exception not success
     */
    private void init() throws Exception {
        try {
            // set port parameters
            serialPort.setSerialPortParams(speed, dataBits, stopBits, parity);
        } catch (UnsupportedCommOperationException e) {
            getExceptionListener("Inposible to determine parameters of port " + serialPortName + "." + e.getMessage());
        }
    }
    //******************************************************************************************************************
    //******************************************************************************************************************
    //********************************* Параметры порта ****************************************************************
    /**
     * RU:<br>
     * Скорость порта. <br>
     * EN:<br>
     * Port speed
     */
    private Integer speed = 9600;

    public Integer getSpeed() {
        return speed;
    }

    @Override
    public void setSpeed(Integer speed) {
        this.speed = speed;
    }
    /**
     * RU:<br>
     * Биты данных.<br>
     * EN:<br>
     * Bits of data
     */
    private Integer dataBits = SerialPort.DATABITS_8;

    public Integer getDataBits() {
        return dataBits;
    }

    @Override
    public void setDataBits(Integer dataBits) {
        this.dataBits = dataBits;
    }
    /**
     * RU:<br>
     * Четность.<br>
     * EN:<br>
     * Parity
     */
    private Integer parity = SerialPort.PARITY_EVEN;

    public Integer getParity() {
        return parity;
    }

    @Override
    public void setParity(Integer parity) {
        this.parity = parity;
    }
    /**
     * RU:<br>
     * Стоповые биты.<br>
     * EN:<br>
     * Stop bits
     */
    private Integer stopBits = SerialPort.STOPBITS_2;

    public Integer getStopBits() {
        return stopBits;
    }

    @Override
    public void setStopBits(Integer stopBits) {
        this.stopBits = stopBits;
    }
    //=========================================== Параметры порта ======================================================
    //==================================================================================================================
    //==================================================================================================================

    /**
     * RU:<br>
     * Освободить порт.<br>
     * EN:<br>
     * Free port
     * @throws java.lang.Exception  not success
     */
    @Override
    protected void freePort() throws Exception {
        try {
            if (isBinded()) {
                serialPort.close();
            }
        } catch (Exception ex) {
            getExceptionListener("Make port free error: " + ex.toString());
        }
    }

    @Override
    protected void sendMessage(byte[] message) throws Exception {
        try {
            // write string to serial port
            outputStream.write(message);
        } catch (IOException e) {
            getExceptionListener("Writing to port error " + serialPortName + "." + e.toString());
        }
    }

    /**
     * RU:<br>
     * Это событие приема данных.<br>
     * EN:<br>
     * It's event of recieving data
     * @param event exrended event
     */
    @Override
    public void serialEvent(SerialPortEvent event) {
        // событие выходит за рамки класс и отправляется в вызывающий модуль
        //System.out.println("ФИЧА - public void serialEvent(SerialPortEvent event)");

        //обработчик события приеме данных в классе, реализующим IReceiveListener 
        //можно реализовать примерно так:
        switch (event.getEventType()) {

            case SerialPortEvent.DATA_AVAILABLE:
                if (getReceiveStreamListeners() != null) {
                    getReceiveStreamListeners().actionPerformed(event, inputStream);
                } else {
                    // read data
                    byte[] res = new byte[0];
                    try {
                        while (inputStream.available() != 0) {
                            final byte[] sb = new byte[inputStream.available()];
                            inputStream.read(sb);
                            final byte[] n = Arrays.copyOf(res, sb.length + res.length);
                            System.arraycopy(sb, 0, n, n.length - sb.length, sb.length);
                            res = Arrays.copyOf(n, n.length);
                            Thread.sleep(50);
                        }
                    } catch (IOException ex) {
                        getLoggerListener("Input stream reading error. " + ex, true);
                    } catch (InterruptedException ex) {
                        getLoggerListener("Multithreading error. " + ex, true);
            }
                    // print data
                    if (res.length != 0) {
                        for (IReceiveListener listener : getListeners()) {
                            listener.actionPerformed(event, res);
                        }
                    }
                }
                break;
            default:
                if (getReceiveStreamListeners() != null) {
                    getReceiveStreamListeners().actionPerformed(event);
                } else {
                    for (IReceiveListener listener : getListeners()) {
                        listener.actionPerformed(event);
                    }
                }
                break;
        }

    }
}
