/*
 *  Copyright (C) 2010 Apertum project. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.evgenic.rxtx.serialPort;

import java.util.Collection;

/**
 * RU:
 * Интерфейс для работы с COM-портом.
 * Здесь описаны все методы для использования порта, такие как отправка массива байт в порт,
 * захват порта для получения данных их порта, освобождение порта, управление конфигурированием порта.
 * <br>
 * EN:
 * The interface for work with COM-port
 * Here all methods for port use, such as sending of a file byte in port are described,
 * capture of port for data acquisition from port, port clearing, management of configuring port
 * @author Evgeniy Egorov
 * @version 1.1
 */
public interface ISerialPort {

    /**
     * RU:
     * Выдать в порт массив байт. При этом происходит захват порта и последующее освобождение
     * по завершению выдачи сообщения.
     * <br>
     * EN:
     * To give out in port a file byte. Thus there is a capture of port and the subsequent clearing
     * on end of delivery of the message.
     * @param message RU: Массив байт для выдачи в порт. <br> EN: the array of byte for send in port.
     * @throws Exception  not success
     */
    public void send(byte[] message) throws Exception;

    /**
     * RU:
     * Захватить порт для приема информации. Сам прием реализован как событие(Listener). Если порт захвачен,
     * то действие по выдаче сообщения в порт происходит без первоначального захвата порта и его последующего освобождения.
     * Выдача использует текущий захват порта.
     * Для освобождения порта вызвать free().
     * <br>
     * EN:
     * to Grasp port for information reception. Reception is realised as event (Listener). If the port is grasped,
     * that action on message delivery in port occurs without initial capture of port and its subsequent clearing.
     * Delivery uses current capture of port.
     * For port clearing to cause free ().
     * @param receiveListener RU: Класс, в котором вызовется метод при приеме информации. <br> EN: the Class in which the method will be caused at information reception.
     * @see free ()
     * @throws Exception RU: Исключение при захвата порта. <br> EN: the Exception at port capture.
    
     */
    public void bind(IReceiveListener receiveListener) throws Exception;
    /**
     * RU:
     * Захватить порт для приема информации. Сам прием реализован как событие(Listener). Если порт захвачен,
     * то действие по выдаче сообщения в порт происходит без первоначального захвата порта и его последующего освобождения.
     * Выдача использует текущий захват порта.
     * Событие IReceiveStreamListener доминирует над списком событий IReceiveListener
     * Для освобождения порта вызвать free().
     * <br>
     * EN:
     * to Grasp port for information reception. Reception is realised as event (Listener). If the port is grasped,
     * that action on message delivery in port occurs without initial capture of port and its subsequent clearing.
     * Delivery uses current capture of port.
     * Event IReceiveStreamListener override the list of events IReceiveStreamListener.
     * For port clearing to cause free ().
     * @param receiveStreamListener RU: Класс, в котором вызовется метод при приеме информации. <br> EN: the Class in which the method will be caused at information reception.
     * @see free ()
     * @throws Exception RU: Исключение при захвата порта. <br> EN: the Exception at port capture.
    
     */
    public void bind(IReceiveStreamListener receiveStreamListener) throws Exception;

    /**
     * RU:
     * Добавить событие при приеме индормации.
     * <br>
     * EN:
     * Add the event for receive information.
     *
     * @param receiveListener RU: Класс, в котором вызовется метод при приеме информации. <br> EN: the Class in which the method will be caused at information reception.
     */
    public void addListener(IReceiveListener receiveListener);

    /**
     * RU:
     * Удалить событие при приеме индормации.
     * <br>
     * EN:
     * Remove the event for receive information.
     * @param receiveListener RU: Удаляемый класс, в котором вызовался метод при приеме информации. <br> EN: the removed Class in which the method was caused at information reception.
     */
    public void removeListener(IReceiveListener receiveListener);

    /**
     * RU:
     * Получить коллекцию всех событий, назначенных для приема данных из порта.
     * <br>
     * EN:
     * Get the collection receive events.
     * @return RU: коллекция назначенных событий <br> EN: the collection assign events
     */
    public Collection<IReceiveListener> getListeners();

    /**
     * RU:
     * Получить событие при приеме индормации, читаем из потока.
     * <br>
     * EN:
     * Get the event for receive information, read from stream.
     *
     * @return  receiveStreamListeners RU: Обработчик приема через поток. <br> EN: Handler of event for receive information, read from stream.
     */
    public IReceiveStreamListener getReceiveStreamListeners();

    /**
     * RU:
     * Назначить событие при приеме индормации, читаем из потока.
     * <br>
     * EN:
     * Set the event for receive information, read from stream.
     *
     * @param receiveStreamListeners RU: Класс, в котором вызовется метод при приеме информации. <br> EN: the Class in which the method will be caused at information reception.
     */
    public void setReceiveStreamListeners(IReceiveStreamListener receiveStreamListeners);

    /**
     * RU:
     * Освободить захваченный ранее порт.
     * see #ind(IReceiveListener receiveListener);
     * <br>
     * EN:
     * to Release the port grasped earlier.
     * see #ind (IReceiveListener receiveListener);
     * @throws Exception RU: Исключение при освобождении порта. <br>  EN: Exception the Exception at port clearing.
     */
    public void free() throws Exception;

    /**
     * RU:
     * Скорость порта бит в секунду. Смотреть в спецификации устройства.
     * <br>
     * EN:
     * Speed of port of bats in a second. To look in the device specification.
     * @param speed RU: Скорость(бит/с) <br> EN: Speed (bit per second)
     */
    public void setSpeed(Integer speed);

    /**
     * RU:
     * Биты данных. Возможные значения смотреть в константах. 
     * <br>
     * EN:
     * Bits of the data. Possible values to look in constants. 
     * @param dataBits RU: Биты данных. EN: Bits of the data.
     * @see gnu.io.SerialPort  <br>
     *      public static final int  DATABITS_5 = 5; <br>
     *      public static final int  DATABITS_6 = 6; <br>
     *      public static final int  DATABITS_7 = 7; <br>
     *      public static final int  DATABITS_8 = 8; <br>
     */
    public void setDataBits(Integer dataBits);

    /**
     * RU:
     * Четность. Возможные значения смотреть в константах.
     * <br>
     * EN:
     * Parity. Possible values to look in constants.
     * @param parity RU: Четность. EN: Parity.
     * @see gnu.io.SerialPort  <br>
     *      public static final int  PARITY_NONE  = 0; <br>
     *      public static final int  PARITY_ODD = 1; <br>
     *      public static final int  PARITY_EVEN = 2; <br>
     *      public static final int  PARITY_MARK = 3; <br>
     *      public static final int  PARITY_SPACE = 4; <br>
     */
    public void setParity(Integer parity);

    /**
     * RU:
     * Стоповые биты. Возможные значения смотреть в константах.
     * <br>
     * EN:
     * Stopovye of a bat. Possible values to look in constants.
     * @param stopBits RU: количество стоповых бит <br> EN: quantity стоповых bit
     * @see gnu.io.SerialPort <br>
     *      public static final int  STOPBITS_1 = 1; <br>
     *      public static final int  STOPBITS_2 = 2; <br>
     *      public static final int  STOPBITS_1_5 = 3; <br>
     */
    public void setStopBits(Integer stopBits);

    /**
     * RU:
     * Определение обработчика логирования для работы с СОМ портами.
     * Можно не использовать если логирование не нужно, тогда просто будет выводиться в консоль.
     *  <br>
     * EN:
     * Definition of the output agent of loginning for work with the CATFISH ports.
     * It is possible not to use if loginning it is not necessary, then it is simple будет to be deduced in the console.
     * @param serialLoggerListener RU: Событие логирования. <br> EN: Event of loginning
     */
    public void setLoggerListener(ISerialLoggerListener serialLoggerListener);

    /**
     * RU:
     * Определение обработчика исключительных ситуаций для работы с СОМ портами.
     * Можно не использовать если обработка исключительных ситуаций не нужна, тогда все эксепшены будут Exception.
     * <br>
     * EN:
     * Definition of the output agent of exclusive situations for work with the COM ports.
     * It is possible not to use if processing of exclusive situations is not necessary, then all exceptions will be Exception.
     * @param serialExceptionListener RU: Событие исключительных ситуация. <br> EN: Event exclusive situation
     */
    public void setExceptionListener(ISerialExceptionListener serialExceptionListener);

    /**
     * RU:
     * Маркировка СОМ-порта, например COM1 или rttyS001.
     * <br>
     * EN:
     * COM-PORT Marks, for example COM1 or rttyS001.
     * @return RU: строка названия СОМ-порта. <br> EN: a line of the name of COM-PORT.
     */
    public String getName();
}
