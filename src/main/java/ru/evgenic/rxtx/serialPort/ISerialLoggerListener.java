/*
 *  Copyright (C) 2010 Apertum project. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.evgenic.rxtx.serialPort;

/**
 * RU:
 * Листенер для обработки логирования при работе с СОМ портом.
 * Имеет единственный метод, в который передасться сообщение из события.
 * При необходимости логирования работы с портом инициализировать это событие вызвав метод
 * из ISerialPort public void setLoggerListener(ISerialLoggerListener serialLoggerListener);
 * <br>
 * EN:
 * Listener for processing of loging at work with the COM port.
 * Has a unique method in which the message from event will be transferred.
 * If necessary of loginning works with port to initialize this event having caused a method
 * from ISerialPort public void setLoggerListener (ISerialLoggerListener serialLoggerListener);
 * @version 1.0
 * @author Evgeniy Egorov
 */
public interface ISerialLoggerListener {

    /**
     * RU:
     * Выполняется при наступлении события.
     * <br>
     * EN:
     * It is carried out at event approach.
     * @param message RU: Сообщение о событии <br>. EN: the Message on event.
     * @param isError RU: Показывает является ли это событие реакцией на ошибку. <br> EN: Shows this event by reaction to an error is.
     */
    public void actionPerformed(String message, boolean isError);
}
