/*
 *  Copyright (C) 2010 Apertum project. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.evgenic.rxtx.serialPort;

import gnu.io.SerialPortEvent;
import java.io.InputStream;

/**
 * RU:
 * Событие приема данных.
 * Имеет единственный метод, в который передасться входной поток и само событие.
 * Этот листенер передается при захвате порта методом из IserialPort public void bind(IReceiveListener receiveListener) throws Exception;
 * <br>
 * EN:
 * Event of reception of the data.
 * Has a unique method, in which pass the input stream  and event.
 * This listener is transferred at capture of port by a method from IserialPort public void bind (IReceiveListener receiveListener) throws Exception;
 * @version 1.1
 * @author Evgeniy Egorov
 */
public interface IReceiveStreamListener {

    /**
     * RU:
     * Выполняется при наступлении события приема данных.
     * <br>
     * EN:
     * It is carried out at approach of event of reception of the data.
     * @param event RU: Для передачи наступившего события. <br> EN: For transfer of the come event.
     * @param inputStream RU: Полученная строка из порта. <br> EN: the Received line from port.
     */
    public void actionPerformed(SerialPortEvent event, InputStream inputStream);

    /**
     * RU:
     * Выполняется при наступлении события, отличного от приема данных включая событие с пустым буфером.
     * Идентификаторы события:
     * <br>
     * EN:
     * It is carried out at approach of the event which are distinct from reception of the data including event with the empty buffer.
     * event Identifiers:
     * <br> <br>
     * SerialPortEvent.BI <br>
     * SerialPortEvent.OE <br>
     * SerialPortEvent.FE <br>
     * SerialPortEvent.PE <br>
     * SerialPortEvent.CD <br>
     * SerialPortEvent.CTS <br>
     * SerialPortEvent.DSR <br>
     * SerialPortEvent.RI <br>
     * SerialPortEvent.OUTPUT_BUFFER_EMPTY <br> <br>
     * RU: Признак события определяется event.getEventType(). <br>
     * EN: the event Sign is defined event.getEventType ().
     *
     * @param event RU: Для передачи наступившего события. <br> EN: For transfer of the come event.
     */
    public void actionPerformed(SerialPortEvent event);
}
