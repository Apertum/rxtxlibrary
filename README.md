#Work with serial port using library RXTX-2.1.7.#

RXTXLibrary.jar - library for communication with rxtx.

test - source of auto-test jUnit4, demonstrating work with library

src - source textxs of library

javadoc - documentation for code

##Strart to work.##

*1. To enable RXTX.
  Copy nesessary dll or so in appropriate places according orders в RXTX.

*2. Include jar-files to your project (RXTXLibrary.jar и RXTXcomm.jar).

*3.  You can start to work with ports similar to its use in RxtxSerialPortTest.java.
    Example:

Send message "Test Message" in port COM2
                
```
#!java

ISerialPort port = new RxtxSerialPort("COM2");
port.send("Test Message".getBytes());
```


Capture port "COM3" for recieving data. After getting data output to console:
                
```
#!java

ISerialPort port = new RxtxSerialPort("COM3");
port.bind(new IReceiveListener() {

                            @Override
                            public void actionPerformed(SerialPortEvent event, byte[] data) {
                                String rData = new String(data);
                                System.out.println("Receive message from Port: " + rData);
                            }

                            @Override
                            public void actionPerformed(SerialPortEvent event) {
                                
                            }

});
```


##Options for using the port.##

*1. Use the method ISerialPort.send(byte [] message)
   Send at the port array of bytes. This results in the capture of the port and the subsequent release
   upon completion of send message.

*2. Use the method ISerialPort.bind(IReceiveListener receiveListener)
   To grasp port for information reception. Reception is realised as event (Listener). If the port is grasped,
   that action on message delivery in port occurs without initial capture of port and its subsequent clearing.
   Delivery uses current capture of port.
   For port clearing to cause free ().