������ � COM-������ ��������� ���������� RXTX-2.1.7.

RXTXLibrary.jar - ���������� �������������� � rxtx.
test - ��������� ��������� jUnit4, ��������������� ������ � �����������
src - �������� ������ ����������
javadoc - ������������ �� ����
rxtxLibs - dll � so, ������� ���������� ��� ������ � ��� ������.

��������� � ������.

1. ���������� ���������� RXTX-2.2.
   ���������� ����������� dll ��� so � ��������������� ����� ������ ��������� � RXTX-2.2.
2. ���������� jar-����� � ������ �������(RXTXLibrary.jar � RXTXcomm.jar).
3. ���������� ������������� � ������ RxtxSerialPortTest.java ������ �������� ������ � �������.
    ��� �������:

        ������� ��������� "Test Message" � ���� COM2
                ISerialPort port = new RxtxSerialPort("COM2");
                port.send("Test Message".getBytes());

        ��������� ���� "COM3" ��� ��������� ������, ��� ��������� ������� � �������:
                ISerialPort port = new RxtxSerialPort("COM3");
                port.bind(new IReceiveListener() {

                            @Override
                            public void actionPerformed(SerialPortEvent event, byte[] data) {
                                String rData = new String(data);
                                System.out.println("Receive message from Port: " + rData);
                            }

                            @Override
                            public void actionPerformed(SerialPortEvent event) {
                                
                            }

               });

�������� ������������� �����.

1. ������������ ����� ISerialPort.send(byte[] message)
   ������ � ���� ������ ����. ��� ���� ���������� ������ ����� � ����������� ������������
   �� ���������� ������ ���������.
2. ������������ ����� ISerialPort.bind(IReceiveListener receiveListener)
   ��������� ���� ��� ������ ����������. ��� ����� ���������� ��� �������(Listener). ���� ���� ��������,
   �� �������� �� ������ ��������� � ���� ���������� ��� ��������������� ������� ����� � ��� ������������ ������������.
   ������ ���������� ������� ������ �����.
   ��� ������������ ����� ������� free().


